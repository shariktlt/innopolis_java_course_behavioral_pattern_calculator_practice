package ru.innopolis.k_sharypov.behavioral.calc.commands;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class DivCommand implements Command {
    private  final String name = "div";
    private final String INVALID_ELEMENTS_COUNT_ERROR = "need more than 1 elements";

    @Override
    public String getCommandName() {
        return name;
    }

    /**
     * Divide all items in args
     * @return
     */
    @Override
    public String execute(String[] args, Calculator calc) {
        if(args.length < 2){
            throw new RuntimeException(INVALID_ELEMENTS_COUNT_ERROR);
        }
        Double res = Double.parseDouble(args[0]);
        for(int i = 1; i < args.length; ++i){
            double local = Double.parseDouble(args[i]);
            if(local == 0d){
                throw new RuntimeException("divide by zero not allowed");
            }
            res/=local;
        }
        return res.toString();
    }
}
