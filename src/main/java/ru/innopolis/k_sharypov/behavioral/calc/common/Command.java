package ru.innopolis.k_sharypov.behavioral.calc.common;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;

/**
 * Created by innopolis on 08.11.16.
 */
public interface Command {
    String getCommandName();
    String execute(String[] args, Calculator calc);
}
