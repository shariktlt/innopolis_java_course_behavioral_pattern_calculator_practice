package ru.innopolis.k_sharypov.behavioral.calc.commands;


import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by innopolis on 08.11.16.
 */
public class HelpSystemCommand implements Command {

    private final String name = "help";
    private final String COMMAND_LIST_TITLE = "Command list:\n";

    @Override
    public String getCommandName() {
        return name;
    }

    /**
     * Prints all available commands
     * @param args
     * @param calc
     * @return
     */
    @Override
    public String execute(String[] args, Calculator calc) {
        StringBuilder sb = new StringBuilder();
        sb.append(COMMAND_LIST_TITLE);
        Iterator it = calc.getCommands().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            sb.append(((Command)pair.getValue()).getCommandName()+"\n");
        }
        return sb.toString();
    }
}
