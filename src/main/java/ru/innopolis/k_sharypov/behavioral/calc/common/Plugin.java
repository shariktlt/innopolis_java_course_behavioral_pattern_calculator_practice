package ru.innopolis.k_sharypov.behavioral.calc.common;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;

/**
 * Created by innopolis on 08.11.16.
 */
public abstract class Plugin {
    Plugin nextPlugin;

    public void registerNext(Plugin nextPlugin) {
        this.nextPlugin = nextPlugin;
    }

    public abstract void process(String cmd, String[] args, String res, Calculator calc);

    public void run(String cmd, String[] args, String res, Calculator calc){
        process(cmd, args, res, calc);
        if(nextPlugin!=null){
            nextPlugin.run(cmd, args, res, calc);
        }
    }
}
