package ru.innopolis.k_sharypov.behavioral.calc.plugin;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Plugin;

/**
 * Created by innopolis on 08.11.16.
 */
public class MonitoringPlugin extends Plugin {

    /**
     * Report to stdout
     * @param cmd
     * @param args
     * @param res
     * @param calc
     */
    @Override
    public void process(String cmd, String[] args, String res, Calculator calc) {
        System.out.format("Dev monitor: %s = %s\n", cmd, res);
    }


}
