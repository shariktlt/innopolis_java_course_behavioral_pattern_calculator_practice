package ru.innopolis.k_sharypov.behavioral.calc;



import ru.innopolis.k_sharypov.behavioral.calc.common.Command;
import ru.innopolis.k_sharypov.behavioral.calc.common.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class Calculator {
    private final String CMD_PROMPT = "calc> ";
    private final String PLUGIN_PACKAGE = "plugin.";
    private final String COMMAND_PACKAGE = "commands.";
    private final String ROOT_PACKAGE = "ru.innopolis.k_sharypov.behavioral.calc.";
    private Map<String,Command> commands = new HashMap<>();
    private Plugin pluginsChain;
    private final String[] systemCommandsStringList = {"HelpSystemCommand","ExitSystemCommand"};
    private final String[] systemPluginsStringList = {"LicencePlugin","MonitoringPlugin"};
    private String serial;
    private boolean isLicenced = false;
    private boolean isRun = true;
    private Config config;


    /**
     * Entry point to calculator
     * @param config
     */

    public void init(Config config){
        configure(config);
        try(InputStreamReader is = new InputStreamReader(System.in); BufferedReader br = new BufferedReader(is)){
            String line = null;
            while(isRun){
                System.out.print(CMD_PROMPT);
                line = br.readLine();
                try {
                    if (!line.isEmpty()) {
                        String[] args = line.split(" ");
                        runCommand(args);
                    }
                }catch (Exception e){
                    System.out.println("Error: "+e.getMessage());
                    //  e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void setCommands(String[] args){
        for(String item: args){
            safeLoadCommand(COMMAND_PACKAGE +item);
        }
    }

    private void safeLoadCommand(String name){
        try{
            Command command =  loadModule(name, Command.class);
            commands.put(command.getCommandName(), command);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void configure(Config config){
        this.config = config;
        this.serial = config.getSerial();
        this.checkLicence();
        this.setCommands(config.getCommands());
        this.setCommands(systemCommandsStringList);
        this.setPlugins(config.getPlugins());
        this.setPlugins(systemPluginsStringList);
    }

    private void setPlugins(String[] args) {
        if(args!=null) {
            for (int i = args.length; i > 0; --i) {
                safeLoadPlugin(PLUGIN_PACKAGE + args[i-1]);
            }
        }
    }

    private void safeLoadPlugin(String name) {
        try{
            Plugin plugin =  loadModule(name, Plugin.class);
            plugin.registerNext(this.pluginsChain);
            pluginsChain = plugin;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void checkLicence() {
        if(serial != null){
            isLicenced = true;
        }
    }



    private void runCommand(String[] args) {
        if(args.length==0){
            return;
        }
        String cmd = args[0];
        String[] arg = Arrays.copyOfRange(args, 1, args.length);
        String res = executeCommand(cmd, arg);
        System.out.println(res);
        startPluginChain(cmd, arg, res);
    }

    private void startPluginChain(String cmd, String[] args, String res) {
        if(pluginsChain != null){
            pluginsChain.run(cmd, args, res, this);
        }
    }

    private String executeCommand(String cmd, String[] args) {
        Command command = commands.get(cmd);
        if (command == null) {
            throw new RuntimeException("command " + cmd + " not found");
        }
        return command.execute(args, this);
    }

    private <T> T loadModule(String name, final Class<T> type ){
        try{
            return type.cast(Class.forName(ROOT_PACKAGE +name).newInstance());
        } catch(final InstantiationException e){
            throw new IllegalStateException(e);
        } catch(final IllegalAccessException e){
            throw new IllegalStateException(e);
        } catch(final ClassNotFoundException e){
            throw new IllegalStateException(e);
        }
    }

    public Map<String, Command> getCommands() {
        return commands;
    }



    public void setRun(boolean run) {
        isRun = run;
    }

    public boolean isRun() {
        return isRun;
    }

    public String getSerial() {
        return serial;
    }

    public boolean isLicenced() {
        return isLicenced;
    }

    public Config getConfig() {
        return config;
    }
}
