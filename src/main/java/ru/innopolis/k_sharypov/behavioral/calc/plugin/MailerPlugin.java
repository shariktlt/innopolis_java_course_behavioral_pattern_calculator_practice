package ru.innopolis.k_sharypov.behavioral.calc.plugin;


import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Plugin;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by innopolis on 08.11.16.
 */
public class MailerPlugin extends Plugin {
    private boolean isConfigured = false;
    private String from;
    private String to;
    private String login;
    private String password;
    private String host;
    private String port;
    private String subject ;
    private Message message;

    /**
     *  Send report to email
     * @param cmd
     * @param args
     * @param res
     * @param calc
     */
    @Override
    public void process(String cmd, String[] args, String res, Calculator calc) {
        if(!isConfigured){
            config(calc.getConfig().getProperties());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Serial: "+calc.getSerial()+"\n")
                .append("Cmd: "+cmd+"\n")
                .append("Res: "+res);
        sendMessage(sb.toString());
    }

    private void define(Properties cfg){
        from = cfg.getProperty("pattern_calc.mailer.from", "innopolis_java_calc_test@shariktlt.ru");
        to = cfg.getProperty("pattern_calc.mailer.to", "shariktlt@gmail.com");
        login = cfg.getProperty("pattern_calc.mailer.login", "innopolis_java_calc_test@shariktlt.ru");
        password = cfg.getProperty("pattern_calc.mailer.password", "innopolis");
        host = cfg.getProperty("pattern_calc.mailer.host", "smtp.yandex.ru");
        port = cfg.getProperty("pattern_calc.mailer.port", "465");
        subject = cfg.getProperty("pattern_calc.mailer.subject", "[pattern_calc] Mailer report");
    }

    private void config(Properties cfg){
        define(cfg);
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(login, password);
                    }
                });
        try {
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        isConfigured = true;
    }



    private void sendMessage(String message) {

        try {
            this.message.setText(message);
            Transport.send(this.message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }
}
