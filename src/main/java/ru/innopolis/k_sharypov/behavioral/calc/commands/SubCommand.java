package ru.innopolis.k_sharypov.behavioral.calc.commands;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class SubCommand implements Command {

    private final String name = "sub";
    private final String ONLY_ONE_ELEMENT_ERROR_STR = "need 2 or more elements";

    @Override
    public String getCommandName() {
        return name;
    }

    /**
     * Subtract items in args, need more than 1 element
     * @param args
     * @param calc
     * @return
     */
    @Override
    public String execute(String[] args, Calculator calc) {
        if(args.length < 2){
            throw new RuntimeException(ONLY_ONE_ELEMENT_ERROR_STR);
        }
        Double res = Double.parseDouble(args[0]);
        for(int i = 1; i<args.length; ++i){
            res-=Double.parseDouble(args[i]);
        }
        return res.toString();
    }
}
