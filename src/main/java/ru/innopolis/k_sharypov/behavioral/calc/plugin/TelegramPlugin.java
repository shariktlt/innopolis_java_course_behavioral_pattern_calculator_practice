package ru.innopolis.k_sharypov.behavioral.calc.plugin;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Plugin;


import java.util.Properties;

/**
 * Created by innopolis on 08.11.16.
 */
public class TelegramPlugin extends Plugin {
    private TelegramBot bot;
    private boolean isConfigured = false;
    private Integer target;

    /**
     * Report to telegram group
     * @param cmd
     * @param args
     * @param res
     * @param calc
     */

    @Override
    public void process(String cmd, String[] args, String res, Calculator calc) {
        if(!isConfigured){
            configure(calc.getConfig().getProperties());
        }
        StringBuffer msg = new StringBuffer("Serial: ")
                .append(calc.getSerial())
                .append("\n")
                .append("Cmd: "+cmd+"\n")
                .append("Res: "+res);
        bot.sendMessage(target, msg.toString(),false, null, null);
    }

    private void configure(Properties properties) {
        bot = TelegramBotAdapter.build(properties.getProperty("pattern_calc.telegram_api_key", "292947394:AAFMdgSJfh19wi675IYLsoKkJfHxcb3iUD0"));
        target = Integer.parseInt(properties.getProperty("pattern_calc.telegram_target", "-158428716"));
    }

}
