package ru.innopolis.k_sharypov.behavioral.calc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by innopolis on 08.11.16.
 */
public class Config {
    private String[] plugins;
    private String[] commands;
    private final String propFileName = "pattern_calc.properties";
    private String serial;
    private Properties properties;

    /**
     * Construct config, load and parse pattern_calc.properties
     */
    public Config() {
        try(InputStream is = getClass().getClassLoader().getResourceAsStream(propFileName)){
            Properties prop = new Properties();
            prop.load(is);
            this.plugins = loadArray(prop.getProperty("pattern_calc.plugins", ""));
            this.commands = loadArray(prop.getProperty("pattern_calc.commands", ""));
            this.serial = prop.getProperty("pattern_calc.serial");
            properties = prop;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String[] loadArray(String string){
        String[] arr = null;
        if( string != null && !(string).isEmpty()){
            arr = string.split(",");
        }
        return arr;
    }

    public String[] getPlugins() {
        return plugins;
    }

    public String[] getCommands() {
        return commands;
    }

    public String getPropFileName() {
        return propFileName;
    }

    public String getSerial() {
        return serial;
    }

    public Properties getProperties() {
        return properties;
    }
}
