package ru.innopolis.k_sharypov.behavioral.calc.commands;


import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class ExitSystemCommand implements Command {

    private final String name = "exit";
    private final String EXIT_MESSAGE = "Exit now";

    @Override
    public String getCommandName() {
        return name;
    }

    /**
     * Switch isRun flag of calculator, to exit in next while iteration
     * @param args
     * @param calc
     * @return
     */
    @Override
    public String execute(String[] args, Calculator calc) {
        calc.setRun(false);
        return EXIT_MESSAGE;
    }
}
