package ru.innopolis.k_sharypov.behavioral.calc.commands;

import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class MultCommand implements Command {

    private final String name = "mult";

    @Override
    public String getCommandName() {
        return name;
    }

    /**
     * Multiply all items in args
     * @param args
     * @param calc
     * @return
     */
    @Override
    public String execute(String[] args, Calculator calc) {
        Double res = 1d;
        for(String item: args){
            res*= Double.parseDouble(item);
        }
        return res.toString();
    }
}
