package ru.innopolis.k_sharypov.behavioral.calc.plugin;


import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Plugin;

/**
 * Created by innopolis on 08.11.16.
 */
public class LicencePlugin extends Plugin {

    private final String NO_LICENCE_MESSAGE = "You use unlicensed copy, get serial code from your retailer";

    /**
     * Show NO_LICENCE_MESSAGE if calculator is not payed
     * @param cmd
     * @param args
     * @param res
     * @param calc
     */
    @Override
    public void process(String cmd, String[] args, String res, Calculator calc) {
        if(!calc.isLicenced()){
            System.out.println(NO_LICENCE_MESSAGE);
        }
    }
}
