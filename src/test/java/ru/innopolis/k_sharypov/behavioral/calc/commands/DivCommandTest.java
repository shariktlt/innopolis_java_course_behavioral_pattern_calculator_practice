package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class DivCommandTest {
    static Command cmd;
    @BeforeClass
    public static void prepare(){
        cmd = new DivCommand();
    }
    @Test
    public void getCommandName() throws Exception {
        assertEquals("Return 'div' name", "div", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        assertEquals("Check correct answer 4 num", new Double(100).toString(), cmd.execute(new String[]{"100", "1"}, null));
        assertEquals("Check correct answer 1 num", new Double(25).toString(), cmd.execute(new String[]{"100", "2", "2"}, null));
    }

    @Test(expected = RuntimeException.class)
    public void zeroInargs(){
        cmd.execute(new String[]{"1", "0"}, null);
    }

    @Test(expected = RuntimeException.class)
    public void zeroArgLength(){
        cmd.execute(new String[]{}, null);
    }
}