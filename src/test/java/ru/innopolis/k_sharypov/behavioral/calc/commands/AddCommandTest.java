package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class AddCommandTest {
    static Command cmd;
    @BeforeClass
    public static void prepare(){
        cmd = new AddCommand();
    }
    @Test
    public void getCommandName() throws Exception {
        assertEquals("Return 'add' name", "add", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        assertEquals("Check correct answer 4 num", new Double(10).toString(), cmd.execute(new String[]{"1", "2", "3", "4"}, null));
        assertEquals("Check correct answer 1 num", new Double(123).toString(), cmd.execute(new String[]{"123"}, null));
        assertEquals("Check correct answer 0 num", new Double(0).toString(), cmd.execute(new String[]{}, null));
    }

}