package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class SubCommandTest {
    private static Command cmd;

    @BeforeClass
    public static void setUp() throws Exception {
        cmd = new SubCommand();
    }

    @Test
    public void getCommandName() throws Exception {
        assertEquals("sub", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        assertEquals(new Double(2).toString(), cmd.execute(new String[]{"99", "50", "40", "7"}, null));
        assertEquals(new Double(-2).toString(), cmd.execute(new String[]{"12", "14"}, null));
    }

}