package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class MultCommandTest {
    private static Command cmd;
    @BeforeClass
    public static void setUp() throws Exception {
        cmd = new MultCommand();
    }

    @Test
    public void getCommandName() throws Exception {
        assertEquals("mult", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        assertEquals(new Double(10).toString(), cmd.execute(new String[]{"1", "2", "5"}, null));
    }

}