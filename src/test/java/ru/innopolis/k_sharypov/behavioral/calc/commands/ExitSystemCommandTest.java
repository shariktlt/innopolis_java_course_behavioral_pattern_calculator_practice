package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class ExitSystemCommandTest {
    static Command cmd;
    @BeforeClass
    public static void setUp() throws Exception {
        cmd = new ExitSystemCommand();
    }

    @Test
    public void getCommandName() throws Exception {
        assertEquals("exit", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        Calculator calculator = new Calculator();
        assertTrue(calculator.isRun());
        cmd.execute(null, calculator);
        assertFalse(calculator.isRun());
    }

}