package ru.innopolis.k_sharypov.behavioral.calc.commands;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.innopolis.k_sharypov.behavioral.calc.Calculator;
import ru.innopolis.k_sharypov.behavioral.calc.common.Command;

import static org.junit.Assert.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class HelpSystemCommandTest {
    private static Command cmd;
    private static Calculator calc;
    @BeforeClass
    public static void setUp() throws Exception {
        cmd = new HelpSystemCommand();
        calc = new Calculator();

    }

    @Test
    public void getCommandName() throws Exception {
        assertEquals("help", cmd.getCommandName());
    }

    @Test
    public void execute() throws Exception {
        assertFalse(cmd.execute(null, calc).isEmpty());
    }

}